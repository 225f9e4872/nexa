Release Notes for Nexa 1.4.0.0
======================================================

Nexa version 1.4.0.0  is now available from:

  <https://gitlab.com/nexa/nexa/-/releases>

Please report bugs using the issue tracker at gitlab:

  <https://gitlab.com/nexa/nexa/-/issues>

This is minor release of Nexa, for more information about Nexa see:

- https://nexa.org
- https://spec.nexa.org

Main changes in 1.4.0.0
-----------------------

This is list of the main changes that have been merged in this release:

- add token wallet for nexad and new token history page for Nexa-QT
- keep track of token mintage operations
- added two token RPC commands: "authority list" and "authority burn"
- bump rostrum version to 10.0.0
- turn rostrum on by default on linux
- update translation files
- multiple libnexa enhancements
- provide deterministic binaries macOS on apple arm architecture

Commit details
--------------

- `4377ceb35` turn electrum on by default, and only report its on in extversion if it really running (Andrew Stone)
- `965953f12` Pop up a warning if the tokendesc database is not updated (Peter Tschipper)
- `e76e7d6d9` Create two new rpc calls for tokens:  "authority list" and "authority burn" (Peter Tschipper)
- `0ff43a89e` Changes to the output when rpc'ing for token balance and mintage (Peter Tschipper)
- `ad7463607` Fix edge case for getting token descriptions (Peter Tschipper)
- `782ce2dbb` Pin rostrum electrum server to v10.0.0 (Andrea Suisani)
- `97dc90499` Bump nexa version 1.4.0.0 (Andrea Suisani)
- `f4e5c0f0d` Initial work needed to have macOS dmg file signed. (Andrea Suisani)
- `db5f03c17` Add token authority tracking to the tokendesc database (Peter Tschipper)
- `5d7a9c7b0` Translations for Authority Tracking (Peter Tschipper)
- `6296f2a6e` Add more message detail when we get a runaway exception (Peter Tschipper)
- `cf2100c25` Create a sync flag for the tokendesc and tokenmint databases (Peter Tschipper)
- `21bcd2b03` Split out token cache classes from grouptokenwallet.h/.cpp (Peter Tschipper)
- `698893fa8` Add -resync startup option and also add it to Nexa-Qt (Peter Tschipper)
- `c2ba92382` Add a token mintage index (Peter Tschipper)
- `fc796131f` Fix bug in GetTokenDescription() (Peter Tschipper)
- `2ba00f9f8` Fix bug in initial sync (Peter Tschipper)
- `f386482cb` Add a Token History page to QT (Peter Tschipper)
- `1ad573c1a` A simpler approach to determining the max network message allowed (Peter Tschipper)
- `23cae0db3` Translations for the token mintage functions (Peter Tschipper)
- `0f1d6d3ed` Fix bug: When wallet is encrypted no tokens could be sent. (Peter Tschipper)
- `89b027554` put a length byte in front of the bloom data coming from the higher level languages so that we can have variable-length filtered data (Andrew Stone)
- `395789032` Cashlib enhancements (Griffith)
- `083ae51d9` Conditionally include `wallet/grouptokenwallet.h` header (Andrea Suisani)
- `7650645df` Fix importwallet rpc so that we import the correct addressforms (Peter Tschipper)
- `9ceef83c9` allow gettxout RPC to accept an outpoint as its argument, add outpoint field, & always show a few other fields if we have that info.  Finally, fix small incorrect check in txmempool -- it was checking that a write lock is held for an operation that only needs to read (Andrew Stone)
- `ded590852` Updated translation files for Token History page (Peter Tschipper)
- `cf27e3a77` Fix ZMQ bugs for Windows platform (Peter Tschipper)
- `72ff02dea` Add two checkpoints (Peter Tschipper)
- `e42a0c851` Fix small bug in grouptokens.py (Peter Tschipper)
- `393f3c8aa` Fix capd_tests.cpp (Peter Tschipper)
- `a47b6cf72` Use SetBoolArg rather than SoftSetBoolArg when restoring wallet (Peter Tschipper)
- `cd970dda5` add versioning and version function to cashlib (Griffith)
- `12f6e4aab` Fix compile warning in coincointroldialog.cpp (Peter Tschipper)
- `74b523b41` Update translation files (Peter Tschipper)
- `6cdbc822c` Rollback the chain if needed when we reconsiderblock() (Peter Tschipper)
- `fa560beda` Hard Fork 1 - Increase the minimum adaptive block size to 2MB (Peter Tschipper)
- `a3ae48bed` Add a Token wallet to Nexa QT (Peter Tschipper)
- `9e3c9e0cb` depends: update rust to last stable version, 1.73.0 (Andrea Suisani)
- `7e41c58ac` Fix the status bar when syncronizing with the network (Peter Tschipper)
- `c6445decd` Add "unspentcount" to getwalletinfo (Peter Tschipper)
- `64cd7f97a` build, qt: Align frameworks with macOS codesign tool requirements (Hennadii Stepanov)
- `bb431ca3c` Add Wallet Rescan option to QT options dialog (Peter Tschipper)
- `5e8600bc6` build, qt: (Re-)sign package (Hennadii Stepanov)
- `4ffea76c6` gitian: update the descriptors to work with the new apple SDK (Andrea Suisani)
- `f87dadbc2` ci: add a build for Apple ARM based arch to our CI suite (Andrea Suisani)
- `cde6a65a6` Update doc/dependecies.md (Andrea Suisani)
- `bbd21ba57` build: Disable valgrind when building zeromq package in depends (Hennadii Stepanov)
- `dc703c6a4` build: Disable libbsd when building zeromq package in depends (Hennadii Stepanov)
- `5d8df2832` zeromq 4.3.4 (fanquake)
- `014474a48` build: use patch rather than sed in zeromq package (fanquake)
- `61fb658a8` build: Drop ZeroMQ patch for glibc < 2.12 (Hennadii Stepanov)
- `34eabb9c0` build: Drop ZeroMQ patch for Mingw-w64 < 4.0 (Hennadii Stepanov)
- `8c03c845d` depends: bump boost version to 1.83.0 (Andrea Suisani)
- `5db26bd7e` build: Use newest `config.{guess,sub}` available for rsm and univalue subsys (Andrea Suisani)
- `37bb0aebc` build: make sure we can overwrite config.{guess,sub} (0xb10c)
- `4c14754f3` build: Use newest `config.{guess,sub}` available (Hennadii Stepanov)
- `22f03a5ac` autogen.sh: warn about needing autoconf if autoreconf is not found (Andrés G. Aragoneses)
- `e3db2f3c1` depends: bump libgmp to version 6.3.0 (Andrea Suisani)
- `5b76c699d` Replace $(AT) with .SILENCE. (Dmitry Goncharov)
- `3e7fea8d6` Revert "build: do not build boost chrono subsystem" (Andrea Suisani)
- `cf2fee82f` ci: use `x86_64-linux-gnu' rather than `x86_64-unknown-linux-gnu` (Andrea Suisani)
- `96f0a66fd` depends: add also Linux 32/64 bit triplet to the list of available archs (Andrea Suisani)
- `7fa51a8ee` ci: update CI conf to work with the new MacOSX sdk (Andrea Suisani)
- `ebc6d37e9` depends: Remove ccache (fanquake)
- `7586d6f17` depends: Add to README.md that we wupport Apple ARM arch (Andrea Suisani)
- `71ac1f953` depends: modernize clang flags (Cory Fields)
- `957c97b7a` depends: remove also log file on make clean (Andrea Suisani)
- `fbf918bdb` depends: use correct triplet name for Intel apple arch (Andrea Suisani)
- `b53b67dfe` build: Fix target name (Hennadii Stepanov)
- `c6a7d6d87` depends: qt to link against our in house openssl library (Andrea Suisani)
- `f7396081c` build, qt: Fix handling of `CXX=clang++` when building `qt` package (Hennadii Stepanov)
- `fba53f561` build: No longer need to hack the `PATH` variable in `config.site (Hennadii Stepanov)
- `e76378c05` build: Let the depends build system define a path to `dsymutil` tool (Hennadii Stepanov)
- `b716eedb3` build: No need to provide defaults for darwin-specific tool (Hennadii Stepanov)
- `ba6248b4f` build: Pass missed darwin-specific tools via `config.site` (Hennadii Stepanov)
- `a1c01d370` scripted-diff: Rename INSTALLNAMETOOL -> INSTALL_NAME_TOOL (Hennadii Stepanov)
- `8ac5f3711` build: Pass missed `strip` tool via `config.site` (Hennadii Stepanov)
- `476a6071a` build: Fix x86_64 <-> arm64 cross-compiling for macOS (Hennadii Stepanov)
- `792d62053` build, qt: Use `mkspecs/bitcoin-linux-g++` for all Linux hosts (Hennadii Stepanov)
- `81e4972d6` build: Add objcopy host tool (Hennadii Stepanov)
- `bf476dd7d` build: optimise arm64 darwin qt build using -O1 (fanquake)
- `17dcfd247` build: Bump Qt to 5.15.5 in depends (Hennadii Stepanov)
- `310f06cf9` depends: always use correct ar for win qt (fanquake)
- `496d2d170` depends: modify FastFixedDtoa optimisation flags (fanquake)
- `02d747fa5` build: Use Link Time Optimization for Qt code on Linux (Hennadii Stepanov)
- `49190404e` build: patch around qt duplicate symbol issue (fanquake)
- `a5036db9b` build: pass -fno-lto when building expat (fanquake)
- `436a4a2e6` build, qt: Fix `QMAKE_CXXFLAGS` expression for `mingw32` host (Hennadii Stepanov)
- `430ac9965` build, qt: drop fix_no_printer.patch (Hennadii Stepanov)
- `6e4599840` depends: in qt.mk sort patches in alphabetical order (Andrea Suisani)
- `c408631e6` depends: remove unused qt patch (Andrea Suisani)
- `abce94485` build, qt: use one patch per line in depends/packages/qt.mk (Pavol Rusnak)
- `44a77a108` depends: use the latest version of config.{guess,sub} to cross compile zeromq (Andrea Suisani)
- `36f7fc384` depends: bump qrencode to version 4.1.1 (Andrea Suisani)
- `e1935f988` depends: bump protobuf and native_protobuf version (Andrea Suisani)
- `5f94688c4` depends: make libgpm to build on macOS on apple arm/intel based archs (Andrea Suisani)
- `be037b344` depends: Fully determine path for darwin_{CC,CXX} (Carl Dong)
- `013549205` build: split native_cctools (fanquake)
- `746f8661d` depends: use Clang 15.0.6 for macos cross compilation (Andrea Suisani)
- `145aa8b83` build: pass _WIN32_WINNT=0x0601 when building libevent for Windows (fanquake)
- `7ffb2d248` depends: make openssl cross-compilable for Apple arm based arch (Andrea Suisani)
- `6c52e62aa` build: use -isysroot over --sysroot on macOS (fanquake)
- `d77ce768f` build: use macOS 11 SDK (Andrea Suisani)
- `c782b2d27` depends: specify libc++ header location for darwin (Cory Fields)
- `3d95576af` build: build x86_64 Linux Boost with -fcf-protection=full (fanquake)
- `ee4102775` build: use -fcf-protection=full when building Windows Boost in depends (fanquake)
- `9f5ff3b30` build: do not build boost chrono subsystem (Andrea Suisani)
- `e46c470db` build: remove duplicate -fvisibility=hidden from Boost build (fanquake)
- `876af2b6e` build: don't install Boost cmake config files (fanquake)
- `dc92596c3` build: fix unoptimized libraries in depends (fanquake)
- `d7279f88c` build: don't use cf-protection when targeting arm-apple-darwin (fanquake)
- `a82300321` build: build Boost with -fcf-protection when targeting Darwin (fanquake)
- `caa7c2196` depends: bump darwin clang to 11.1 (Cory Fields)
- `05d56e3a4` depends: boost: Specify cflags+compileflags (Carl Dong)
- `5735ba6bb` depends: boost: Remove unnecessary _archiver_ (Carl Dong)
- `4436ef979` depends: boost: Cleanup toolset selection (Carl Dong)
- `1d110d109` depends: boost: Cleanup architecture/address-model (Carl Dong)
- `91f557793` depends: boost: Disable all compression (Carl Dong)
- `d01b4ee75` depends: boost: Split into non-/native packages (Carl Dong)
- `4b973d88c` depends: Propagate only specific CLI variables to sub-makes (Carl Dong)
- `78f7885ff` Mark print-% target as phony. (Dmitry Goncharov)
- `c6b838e94` build: Add variable printing target to Makefiles (Carl Dong)
- `3ba66d784` depends: boost: Use clang toolset if clang in CXX (Carl Dong)
- `9746a5e0f` depends: boost: Split target-os from toolset (Carl Dong)
- `5bbce30e9` depends: boost: Specify toolset to bootstrap.sh (Carl Dong)
- `59574690a` depends: Propagate well-known vars into depends (Carl Dong)
- `6d57e1b29` depends: update boost to ver 1.77.0 (Andrea Suisani)
- `d02541c13` Fix native_cctools to work with apple arm64 arch (Andrea Suisani)
- `218223505` build: native cctools 973.0.1, ld64 609 (fanquake)
- `c32200541` depends: tar: Always extract as yourself (Carl Dong)
- `cd1bae23a` depends: bump native_cctools for fixed lto with external clang (Cory Fields)
- `8241a3089` Use latest version to config.{guess,sub} for libevent and native_cctools (Andrea Suisani)
- `0a978f744` Update config.{guess,sub} (Andrea Suisani)
- `523c7b467` Revert "Pin rostrum electrum server to v9.0" (Andrea Suisani)
- `f34b9592b` Update or fix tooltips for "Send Coins Entry" dialog (Peter Tschipper)
- `f4e653c31` When melting tokens disallow negative token values (Peter Tschipper)
- `917b8ac88` [ci] use python 3.11 to run rostrum QA functional tests (Andrea Suisani)

Credits
-------

Thanks to everyone who directly contributed to this release:

- Andrea Suisani
- Andrew Stone
- Griffith
- Peter Tschipper

We have backported a set changes from Bitcoin Core.

Following all the indirect contributors whose work has been imported via the above backports:

- 0xb10c
- Andrés G. Aragoneses
- Carl Dong
- Cory Fields
- Dmitry Goncharov
- Hennadii Stepanov
- Pavol Rusnak
- fanquake
