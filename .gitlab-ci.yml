#include:
#    - template: Code-Quality.gitlab-ci.yml

#code_quality:
#    artifacts:
#        paths: [gl-code-quality-report.json]

#GENERAL SETTINGS

# Top-level general rules determine when this pipeline is run:
# - only on merge requests, new tags and changes to dev
# - NOT on any branch except dev
# - will run detached merge request pipelines for any merge request,
#   targeting any branch
# Read more on when to use this template at
# https://docs.gitlab.com/ee/ci/yaml/#workflowrules
# https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/ci/templates/Workflows/MergeRequest-Pipelines.gitlab-ci.yml

workflow:
    rules:
        - if: $CI_MERGE_REQUEST_IID
        - if: $CI_COMMIT_TAG
        - if: $CI_COMMIT_BRANCH == "dev"
        - if: $CI_PIPELINE_SOURCE == "schedule"

stages:
    - static_checks
    - build_depends
    - build
    - build_tests
    - qa_tests
    - benchmark_tests

cache: &global_cache_settings
    paths:
        - ccache/

###################
# Gitian binaries #
###################
.docker_common:
    stage: build
    # Environment based on workarounds in this issue:
    # https://gitlab.com/gitlab-org/gitlab-runner/-/issues/27300
    image: bchunlimited/nexa:ubuntu20.04
    cache: {}
    variables:
        DOCKER_HOST: tcp://docker:2375
        DOCKER_DRIVER: overlay2
        DOCKER_TLS_CERTDIR: ""
        DEBIAN_FRONTEND: noninteractive
    services:
        - name: docker:dind
          alias: docker
          command: ["--tls=false"]
    rules:
        - if: ($CI_PIPELINE_SOURCE == "schedule" && $GITIAN == "True")


gitian-binaries:
    stage: build
    extends: .docker_common
    script:
        - apt-get update && apt-get -y --no-install-recommends --no-upgrade -qq install openssh-client apt-cacher-ng ruby docker.io
        - git clone https://github.com/devrandom/gitian-builder.git
        - cd gitian-builder
        - bin/make-base-vm --suite focal --arch amd64 --docker > make-base-vm.log  2>&1
        - export USE_DOCKER=1
        - mkdir result
        - bin/gbuild -j `nproc` -m 6000 --url nexa=$CI_REPOSITORY_URL --commit nexa=$CI_DEFAULT_BRANCH $CI_PROJECT_DIR/contrib/gitian-descriptors/gitian-linux-x86.yml > gbuild.log  2>&1
    artifacts:
        when: always
        paths:
            - ./gitian-builder/build/out/*
            - ./gitian-builder/result/*.yaml
            - ./gitian-builder/var/build.log
            - ./gitian-builder/var/build-script
    tags:
        - scheduled
    rules:
        - if: ($CI_PIPELINE_SOURCE == "schedule" && $GITIAN == "True")

######################################################
######## Ubuntu base settings
######################################################
.ubuntu_base:
    image: bchunlimited/nexa:ubuntu20.04

.ubuntu_ccache_scripts:
    extends: .ubuntu_base
    before_script:
        - mkdir -p ccache
        - export CCACHE_BASEDIR=${PWD}
        - export CCACHE_DIR=${PWD}/ccache
        - export CCACHE_COMPILERCHECK=content
        # Limit ccache to 3 GB (from default 5 GB).
        - ccache -M 3G
        - ccache --zero-stats || true

######################################################
######## Linting
######################################################
.ubuntu_cache-linting:
    extends: .ubuntu_ccache_scripts
    cache:
        <<: *global_cache_settings
        key: linting_cache

check-formatting:
    stage: static_checks
    extends: .ubuntu_cache-linting
    needs: []
    script:
        - apt-get install -y wget gpg-agent
        - wget https://apt.llvm.org/llvm.sh
        - chmod +x llvm.sh
        - ./llvm.sh 15
        - apt-get update
        - apt-get install -y clang-format-15
        - ./autogen.sh
        - ./configure --enable-glibc-back-compat --enable-reduce-exports
        - make check-formatting
    rules:
      - if: $CI_PIPELINE_SOURCE != "schedule"

######################################################
######## x86_64 Linux + deps as via system lib
######################################################
.cache-ubuntu-nodeps:
    extends: .ubuntu_ccache_scripts
    cache:
        <<: *global_cache_settings
        key: ubuntu_cache-nodeps

build-ubuntu-nodeps:
    stage: build
    extends: .cache-ubuntu-nodeps
    needs: []
    script:
        - ./autogen.sh
        - mkdir build; cd build
        - ../configure --cache-file=config.cache --with-gui=no --enable-glibc-back-compat --enable-reduce-exports
        - make -j `nproc`
        - ccache --show-stats
    artifacts:
        when: always
        paths:
            - ./build/src/nexad
            - ./build/src/nexa-cli
            - ./build/src/nexa-miner
            - ./build/src/nexa-tx
            - ./build/src/test/test_nexa
            - ./build/src/bench/bench_nexa
            - ./build/config.log #in case of configure failure
    rules:
      - if: $CI_PIPELINE_SOURCE != "schedule"

build-ubuntu-nodeps-qt:
    stage: build
    extends: .cache-ubuntu-nodeps
    needs: []
    script:
        - ./autogen.sh
        - mkdir build; cd build
        - ../configure --cache-file=config.cache --with-gui=qt5 --enable-glibc-back-compat --enable-reduce-exports --disable-bench --disable-tests
        - make -j `nproc`
        - ccache --show-stats
    artifacts:
        when: on_failure
        paths:
            - ./build/config.log
    rules:
      - if: $CI_PIPELINE_SOURCE != "schedule"

build-ubuntu-tests-nodeps:
    stage: build_tests
    extends: .cache-ubuntu-nodeps
    needs: ["build-ubuntu-nodeps"]
    cache: {}
    script:
        - (cd build/src; ./test/test_nexa)
    dependencies:
        - build-ubuntu-nodeps
    rules:
      - if: $CI_PIPELINE_SOURCE != "schedule"


######################################################
######## Linux 64 bit, source deps
######################################################
.cache-ubuntu:
    extends: .ubuntu_ccache_scripts
    cache:
        <<: *global_cache_settings
        key: ubuntu_cache
        paths:
            - ccache/
            - depends/sources
            - depends/built
            - depends/x86_64-linux-gnu

build-ubuntu-deps:
    stage: build_depends
    extends: .cache-ubuntu
    needs: []
    script:
        - cd depends
        - make HOST=x86_64-linux-gnu -j `nproc`
    artifacts:
        paths:
            - depends/x86_64-linux-gnu
    rules:
      - if: ($CI_PIPELINE_SOURCE != "schedule" || $EXTENDED_TESTS == "True")

build-ubuntu:
    stage: build
    extends: .cache-ubuntu
    needs: [build-ubuntu-deps]
    script:
        - ./autogen.sh
        - mkdir build; cd build
        - ../configure --enable-shared --enable-debug --enable-zmq --enable-glibc-back-compat --enable-reduce-exports --cache-file=config.cache --prefix=$PWD/../depends/x86_64-linux-gnu CPPFLAGS=-DDEBUG_LOCKORDER
        - make -j `nproc`
        - ccache --show-stats
    dependencies:
        - build-ubuntu-deps
    artifacts:
        when: always
        paths:
            - ./build/src/nexad
            - ./build/src/nexa-cli
            - ./build/src/nexa-miner
            - ./build/src/nexa-tx
            - ./build/src/test/
            - ./build/src/test/test_nexa
            - ./build/src/bench/bench_nexa
            - ./build/qa/*
            - ./build/src/test/*.log
            - ./src/test/*.log
            - ./src/test-suite.log
            - ./build/src/.libs/libnexa.*
    rules:
      - if: ($CI_PIPELINE_SOURCE != "schedule" || $EXTENDED_TESTS == "True")

build-ubuntu-rostrum:
    extends: .ubuntu_base
    stage: build
    cache:
      paths:
        # Paths from Cargo Book, section "Caching the Cargo home in CI"
        - cargohome/bin
        - cargohome/registry/index
        - cargohome/registry/cache
        - cargohome/git/db

    needs: [build-ubuntu-deps]

    variables:
      CARGO_HOME: ${CI_PROJECT_DIR}/cargohome

    before_script:
      - mkdir -p cargohome
      - mkdir -p build

    script:
        # add cargo to PATH
        - export PATH=$PATH:`pwd`/depends/x86_64-linux-gnu/native/bin
        # Prepare make file
        - ./autogen.sh > /dev/null
        - cd build
        - ../configure --cache-file=config.cache
            --prefix=$PWD/../depends/x86_64-linux-gnu > /dev/null

        - make rostrum

    dependencies:
        - build-ubuntu-deps

    artifacts:
        paths:
            - ./build/src/rostrum
    rules:
      - if: $CI_PIPELINE_SOURCE != "schedule"

build-ubuntu-tests:
    stage: build_tests
    extends: .cache-ubuntu
    needs: ["build-ubuntu"]
    cache: {}
    script:
        - (cd build/src; ./test/test_nexa)
    artifacts:
        when: on_failure
        paths:
            - ./build/src/test/
    dependencies:
        - build-ubuntu
    rules:
      - if: $CI_PIPELINE_SOURCE != "schedule"

test-ubuntu-qa:
    stage: qa_tests
    extends: .cache-ubuntu
    needs: ["build-ubuntu"]
    script:
        - mkdir -p $CI_PROJECT_DIR/cores
        - mkdir -p $CI_PROJECT_DIR/saved-cores
        - echo $CI_PROJECT_DIR/cores/core.%e.%p.%h.%t | tee /proc/sys/kernel/core_pattern
        - cd build;
        - ./qa/pull-tester/rpc-tests.py --coverage --no-ipv6-rpc-listen --gitlab
    dependencies:
        - build-ubuntu
    artifacts:
        when: on_failure
        paths:
            - ./qa/qa_tests/
            - $CI_PROJECT_DIR/saved-cores
            - ./build/src/nexad
            - ./build/ctorout.txt
    rules:
      - if: $CI_PIPELINE_SOURCE != "schedule"

test-ubuntu-qa-extended:
    stage: qa_tests
    extends: .cache-ubuntu
    needs: ["build-ubuntu"]
    script:
        - mkdir -p $CI_PROJECT_DIR/cores
        - mkdir -p $CI_PROJECT_DIR/saved-cores
        - echo $CI_PROJECT_DIR/cores/core.%e.%p.%h.%t | tee /proc/sys/kernel/core_pattern
        - cd build;
        - ./qa/pull-tester/rpc-tests.py --coverage --no-ipv6-rpc-listen --gitlab --extended-only
    dependencies:
        - build-ubuntu
    artifacts:
        when: on_failure
        paths:
            - ./qa/qa_tests/
            - $CI_PROJECT_DIR/saved-cores
            - ./build/src/nexad
            - ./build/ctorout.txt
    tags:
        - scheduled
    rules:
        - if: ($CI_PIPELINE_SOURCE == "schedule" && $EXTENDED_TESTS == "True")


test-ubuntu-benchmarks:
    extends: .ubuntu_base
    stage: benchmark_tests
    needs: ["build-ubuntu-nodeps"]
    script:
        - (./build/src/bench/bench_nexa -evals=1)
    dependencies:
        - build-ubuntu-nodeps
    rules:
      - if: $CI_PIPELINE_SOURCE != "schedule"

test-ubuntu-qa-rostrum:
    stage: qa_tests
    image: python:3.11
    needs: ["build-ubuntu", "build-ubuntu-rostrum"]
    variables:
        PIP_CACHE_DIR: $CI_PROJECT_DIR/cache-pip
    cache:
        key: $CI_JOB_NAME
        paths:
            - cache-pip
    before_script:
        - pip install -q psutil
    script:
        - mkdir -p $CI_PROJECT_DIR/cores
        - mkdir -p $CI_PROJECT_DIR/saved-cores
        - echo $CI_PROJECT_DIR/cores/core.%e.%p.%h.%t | tee /proc/sys/kernel/core_pattern

        - export NODE_PATH=`pwd`/build/src/nexad
        - export ROSTRUM_PATH=`pwd`/build/src/rostrum
        - wget https://gitlab.com/bitcoinunlimited/rostrum/-/archive/v10.0.0/rostrum-v10.0.0.tar.gz -O rostrum-master.tar.gz
        - tar -zxf rostrum-master.tar.gz
        - ./rostrum-v10.0.0/test/functional/test_runner.py --no-ipv6-rpc-listen --gitlab
    dependencies:
        - build-ubuntu
        - build-ubuntu-rostrum
    artifacts:
        when: on_failure
        paths:
            - ./qa/qa_tests/
            - $CI_PROJECT_DIR/saved-cores
            - ./build/src/nexad
            - ./build/src/rostrum
            - ./build/ctorout.txt
    rules:
      - if: $CI_PIPELINE_SOURCE != "schedule"

######################################################
######## nexad clang (no depend, only system lib installed via apt)
######################################################
.cache-ubuntu-clang:
    extends: .ubuntu_ccache_scripts
    cache:
        <<: *global_cache_settings
        key: ubuntu_cache_clang

.ubuntu-clang-env:
    extends: .cache-ubuntu-clang
    variables:
        CC: clang-12
        CXX: clang++-12

build-ubuntu-clang:
    stage: build
    extends: .ubuntu-clang-env
    needs: []
    script:
        - ./autogen.sh
        - mkdir build; cd build
        - ../configure --cache-file=config.cache --enable-zmq --with-gui=qt5 CPPFLAGS=-DDEBUG_LOCKORDER
        - make -j `nproc`
        - ccache --show-stats
    artifacts:
        paths:
            - ./build/src/nexad
            - ./build/src/nexa-cli
            - ./build/src/nexa-miner
            - ./build/src/nexa-tx
            - ./build/src/test/
            - ./build/src/test/test_nexa
            - ./build/src/test/*.log
            - ./src/test/*.log
            - ./src/test-suite.log
    rules:
      - if: $CI_PIPELINE_SOURCE != "schedule"

build-ubuntu-tests-clang:
    stage: build_tests
    extends: .ubuntu-clang-env
    needs: ["build-ubuntu-clang"]
    cache: {}
    script:
        - (cd build/src; ./test/test_nexa)
    artifacts:
        when: on_failure
        paths:
            - ./build/src/test/
    dependencies:
        - build-ubuntu-clang
    rules:
      - if: $CI_PIPELINE_SOURCE != "schedule"

######################################################
######## ARM64
######################################################
.cache-arm-64:
    extends: .ubuntu_ccache_scripts
    cache:
        <<: *global_cache_settings
        key: arm_cache-64
        paths:
            - ccache/
            - depends/sources
            - depends/built
            - depends/aarch64-linux-gnu

build-arm-depends-64:
    stage: build_depends
    extends: .cache-arm-64
    script:
        - cd depends
        - make HOST=aarch64-linux-gnu NO_QT=1 -j `nproc` NO_RUST=1
    artifacts:
        paths:
            - depends/aarch64-linux-gnu
    rules:
      - if: $CI_PIPELINE_SOURCE != "schedule"

build-arm-64:
    stage: build
    extends: .cache-arm-64
    needs: ["build-arm-depends-64"]
    script:
        - ./autogen.sh
        - mkdir build; cd build
        - ../configure --prefix=$PWD/../depends/aarch64-linux-gnu --enable-glibc-back-compat --enable-reduce-exports CXXFLAGS=-Wno-psabi
        - make -j `nproc`
        - ccache --show-stats
    dependencies:
        - build-arm-depends-64
    artifacts:
        when: on_failure
        paths:
            - ./build/config.log
    rules:
      - if: $CI_PIPELINE_SOURCE != "schedule"

######################################################
######## ARM32
######################################################
.cache-arm-32:
    extends: .ubuntu_ccache_scripts
    cache:
        <<: *global_cache_settings
        key: arm_cache-32
        paths:
            - ccache/
            - depends/sources
            - depends/built
            - depends/arm-linux-gnueabihf

build-arm-depends-32:
    stage: build_depends
    extends: .cache-arm-32
    script:
        - cd depends
        - make HOST=arm-linux-gnueabihf NO_QT=1 -j `nproc` NO_RUST=1
    artifacts:
        paths:
            - depends/arm-linux-gnueabihf
    rules:
      - if: $CI_PIPELINE_SOURCE != "schedule"

build-arm-32:
    stage: build
    extends: .cache-arm-32
    needs: ["build-arm-depends-32"]
    script:
        - ./autogen.sh
        - mkdir build; cd build
        - ../configure --prefix=$PWD/../depends/arm-linux-gnueabihf --enable-glibc-back-compat --enable-reduce-exports
        - make -j `nproc`
        - ccache --show-stats
    dependencies:
        - build-arm-depends-32
    artifacts:
        when: on_failure
        paths:
            - ./build/config.log
    rules:
      - if: $CI_PIPELINE_SOURCE != "schedule"

######################################################
######## Win64
######################################################
.cache-win-64:
    extends: .ubuntu_ccache_scripts
    cache:
        <<: *global_cache_settings
        key: win_cache-64
        paths:
            - ccache/
            - depends/sources
            - depends/built
            - depends/x86_64-w64-mingw32

build-win-64-depends:
    stage: build_depends
    extends: .cache-win-64
    script:
        - cd depends
        - make HOST=x86_64-w64-mingw32 NO_QT=1 -j `nproc` NO_RUST=1
    artifacts:
        paths:
            - depends/x86_64-w64-mingw32
    rules:
      - if: $CI_PIPELINE_SOURCE != "schedule"

build-win-64:
    stage: build
    extends: .cache-win-64
    needs: ["build-win-64-depends"]
    script:
        - ./autogen.sh
        - - mkdir build; cd build
        - ../configure --enable-reduce-exports --prefix=$PWD/../depends/x86_64-w64-mingw32
        - make -j `nproc`
        - ccache --show-stats
    dependencies:
        - build-win-64-depends
    rules:
      - if: $CI_PIPELINE_SOURCE != "schedule"

######################################################
######## macOS Intel X86_64
######################################################
.cache-osx:
    extends: .ubuntu_ccache_scripts
    cache:
        <<: *global_cache_settings
        key: osx_cache
        paths:
            - ccache/
            - depends/sources
            - depends/built
            - depends/x86_64-apple-darwin
    rules:
      - if: $CI_PIPELINE_SOURCE != "schedule"

build-osx-depends:
    stage: build_depends
    extends: .cache-osx
    script:
        - mkdir -p depends/sdk-sources; mkdir depends/SDKs
        - curl --location --fail https://www.bitcoinunlimited.info/sdks/MacOSX11.3.sdk.tar.xz -o ./depends/sdk-sources/MacOSX11.3.sdk.tar.xz
        - tar -C depends/SDKs -xf depends/sdk-sources/MacOSX11.3.sdk.tar.xz
        - cd depends
        - make HOST=x86_64-apple-darwin -j `nproc` NO_RUST=1
        - ccache --show-stats
    artifacts:
        paths:
            - depends/x86_64-apple-darwin
            - depends/SDKs
    rules:
      - if: $CI_PIPELINE_SOURCE != "schedule"

build-osx:
    stage: build
    extends: .cache-osx
    needs: ["build-osx-depends"]
    script:
        - ./autogen.sh
        - mkdir build; cd build
        - ../configure --enable-reduce-exports --prefix=$PWD/../depends/x86_64-apple-darwin
        - make -j `nproc`
        - ccache --show-stats
    dependencies:
        - build-osx-depends
    artifacts:
        when: always
        paths:
            - ./build/src/.libs/libnexa.*
            - ./build/config.log # on failure
            - ./depends/x86_64-apple-darwin/share/config.site # on failure
    rules:
      - if: $CI_PIPELINE_SOURCE != "schedule"

######################################################
######## macOS ARM 64
######################################################
.cache-osx-arm64:
    extends: .ubuntu_ccache_scripts
    cache:
        <<: *global_cache_settings
        key: osx_cache_arm64
        paths:
            - ccache/
            - depends/sources
            - depends/built
            - depends/arm64-apple-darwin
    rules:
      - if: $CI_PIPELINE_SOURCE != "schedule"

build-osx-arm64-depends:
    stage: build_depends
    extends: .cache-osx-arm64
    script:
        - mkdir -p depends/sdk-sources; mkdir depends/SDKs
        - curl --location --fail https://www.bitcoinunlimited.info/sdks/MacOSX11.3.sdk.tar.xz -o ./depends/sdk-sources/MacOSX11.3.sdk.tar.xz
        - tar -C depends/SDKs -xf depends/sdk-sources/MacOSX11.3.sdk.tar.xz
        - cd depends
        - make HOST=arm64-apple-darwin -j `nproc` NO_RUST=1
        - ccache --show-stats
    artifacts:
        paths:
            - depends/arm64-apple-darwin
            - depends/SDKs
    rules:
      - if: $CI_PIPELINE_SOURCE != "schedule"

build-osx-arm64:
    stage: build
    extends: .cache-osx-arm64
    needs: ["build-osx-arm64-depends"]
    script:
        - ./autogen.sh
        - mkdir build; cd build
        - ../configure --enable-reduce-exports --prefix=$PWD/../depends/arm64-apple-darwin
        - make -j `nproc`
        - ccache --show-stats
    dependencies:
        - build-osx-arm64-depends
    artifacts:
        when: always
        paths:
            - ./build/src/.libs/libnexa.*
            - ./build/config.log # on failure
            - ./depends/arm64-apple-darwin/share/config.site # on failure
    rules:
      - if: $CI_PIPELINE_SOURCE != "schedule"

######################################################
######## Debian base settings
######################################################
.debian_base:
    image: bchunlimited/gitlabci:debian11

.debian_ccache_scripts:
    extends: .debian_base
    before_script:
        - mkdir -p ccache
        - export CCACHE_BASEDIR=${PWD}
        - export CCACHE_DIR=${PWD}/ccache
        - export CCACHE_COMPILERCHECK=content
        - ccache --zero-stats || true

######################################################
######## x86_64 Debian + deps as via system lib
######################################################
.cache-debian-nodeps:
    extends: .debian_ccache_scripts
    cache:
        <<: *global_cache_settings
        key: debian_cache-nodeps

build-debian-nodeps:
    stage: build
    extends: .cache-debian-nodeps
    needs: []
    script:
        - ./autogen.sh
        - mkdir build; cd build
        - ../configure --cache-file=config.cache --with-gui=no --enable-glibc-back-compat --enable-reduce-exports
        - make -j `nproc`
        - ccache --show-stats
    artifacts:
        when: on_failure
        paths:
            - ./build/config.log
    rules:
      - if: $CI_PIPELINE_SOURCE != "schedule"

build-debian-nodeps-qt:
    stage: build
    extends: .cache-debian-nodeps
    needs: []
    script:
        - ./autogen.sh
        - mkdir build; cd build
        - ../configure --cache-file=config.cache --with-gui=qt5 --enable-glibc-back-compat --enable-reduce-exports --disable-bench --disable-tests
        - make -j `nproc`
        - ccache --show-stats
    artifacts:
        when: on_failure
        paths:
            - build/config.log
    rules:
      - if: $CI_PIPELINE_SOURCE != "schedule"
